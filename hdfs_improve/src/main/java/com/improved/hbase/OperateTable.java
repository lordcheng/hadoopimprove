package com.improved.hbase;

import com.improved.model.SmallFileInfo;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class OperateTable {
    public Configuration configuration;
    public Connection connection;
    public Admin admin;

    public Table table;
    private String tableName;

    public OperateTable(String tableName, String[] cols) throws Exception {
        init(tableName, cols);
    }

    // 初始化链接
    public void init(String tableName, String[] cols) throws Exception {
        configuration = HBaseConfiguration.create();
        /*
         * configuration.set("hbase.zookeeper.quorum",
         * "10.10.3.181,10.10.3.182,10.10.3.183");
         * configuration.set("hbase.zookeeper.property.clientPort","2181");
         * configuration.set("zookeeper.znode.parent","/hbase");
         */
        configuration.set("hbase.zookeeper.property.clientPort", "2181");
        configuration.set("hbase.zookeeper.quorum", "master,slave1,slave2,slave3,slave4");
        configuration.set("hbase.master", "master:60000");
        File workaround = new File(".");
        System.getProperties().put("hadoop.home.dir", workaround.getAbsolutePath());
        new File("./bin").mkdirs();
        try {
            new File("./bin/winutils.exe").createNewFile();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            connection = ConnectionFactory.createConnection(configuration);
            admin = connection.getAdmin();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.tableName = tableName;
        if (!admin.tableExists(TableName.valueOf(tableName)))
            createTable(cols);
        table = connection.getTable(TableName.valueOf(tableName));
    }

    // 关闭连接
    public void close() {
        try {
            if (null != admin)
                admin.close();
            if (null != connection)
                connection.close();
            if (null != table)
                table.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    // 建表
    public void createTable(String[] cols) throws IOException {
        if (null != table || admin.tableExists(TableName.valueOf(tableName))) {
            System.out.println("talbe is exists!");
        } else {
            TableName tableName_ = TableName.valueOf(tableName);
            HTableDescriptor hTableDescriptor = new HTableDescriptor(tableName_);
            for (String col : cols) {
                HColumnDescriptor hColumnDescriptor = new HColumnDescriptor(col);
                hTableDescriptor.addFamily(hColumnDescriptor);
            }
            admin.createTable(hTableDescriptor);
            if (admin.tableExists(TableName.valueOf(tableName))) {
                table = connection.getTable(TableName.valueOf(tableName));
            } else {
                System.out.println("faile to create the table!");
            }
        }
    }

    // 删表
    public void deleteTable() throws IOException {
        if (null != table || admin.tableExists(TableName.valueOf(tableName))) {
            TableName tn = TableName.valueOf(tableName);
            admin.disableTable(tn);
            admin.deleteTable(tn);
        } else {
            System.out.println("The table is not exits!");
        }
    }

    // 查看已有表
    public void listTables() throws IOException {
        HTableDescriptor hTableDescriptors[] = admin.listTables();
        for (HTableDescriptor hTableDescriptor : hTableDescriptors) {
            System.out.println(hTableDescriptor.getNameAsString());
        }
    }

    // 插入单个数据
    public void insterRow(String rowkey, String colFamily, String col, String val)
            throws IOException {
        if (isConnectedTable()) {
            Put put = new Put(Bytes.toBytes(rowkey));
            put.addColumn(Bytes.toBytes(colFamily), Bytes.toBytes(col), Bytes.toBytes(val));
            table.put(put);
        }
    }

    //插入多行多列数据
    public void insterRow(String colFamily, String smallFileName,
                          String hdfsPath, String combineFileName, String startSet, String len,
                          HashMap<String, SmallFileInfo> indexs)
            throws IOException {
        if (isConnectedTable()) {
            Iterator iter = indexs.entrySet().iterator();
            List<Put> putList = new ArrayList<Put>();
            Put put = null;
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                String rowkey = (String) entry.getKey();
                SmallFileInfo smallFileInfo = (SmallFileInfo) entry.getValue();
                put = new Put(Bytes.toBytes(rowkey));
                put.addColumn(Bytes.toBytes(colFamily), Bytes.toBytes(smallFileName), Bytes.toBytes(smallFileInfo.getFileName()));
                put.addColumn(Bytes.toBytes(colFamily), Bytes.toBytes(hdfsPath), Bytes.toBytes(smallFileInfo.getPath()));
                put.addColumn(Bytes.toBytes(colFamily), Bytes.toBytes(combineFileName), Bytes.toBytes(smallFileInfo.getCmbFileName()));
                put.addColumn(Bytes.toBytes(colFamily), Bytes.toBytes(startSet), Bytes.toBytes(smallFileInfo.getStartSet()));
                put.addColumn(Bytes.toBytes(colFamily), Bytes.toBytes(len), Bytes.toBytes(smallFileInfo.getSize()));
                putList.add(put);
            }
            table.put(putList);
        }
    }

    // 删除指定rowkey的所有数据
    public void deleRow(String rowkey, String colFamily, String col) throws IOException {
        if (isConnectedTable()) {
            Delete delete = new Delete(Bytes.toBytes(rowkey));
            // 删除指定列族
            // delete.addFamily(Bytes.toBytes(colFamily));
            // 删除指定列
            // delete.addColumn(Bytes.toBytes(colFamily),Bytes.toBytes(col));
            table.delete(delete);
            // 批量删除
         /*
         * List<Delete> deleteList = new ArrayList<Delete>();
         * deleteList.add(delete); table.delete(deleteList);
         */
        }
    }

    // 根据rowkey等查找数据
    public Result getData(String rowkey, String colFamily) throws IOException {
        if (isConnectedTable()) {
            Get get = new Get(Bytes.toBytes(rowkey));
            // 获取指定列族数据
            get.addFamily(Bytes.toBytes(colFamily));
            // 获取指定列数据
            //get.addColumn(Bytes.toBytes(colFamily),Bytes.toBytes(col));
            Result result = table.get(get);
            //showCell(result);
            return result;
        }
        return null;
    }

    // 格式化输出
    public void showCell(Result result) {
        Cell[] cells = result.rawCells();
        for (Cell cell : cells) {
            System.out.println("RowName:" + new String(CellUtil.cloneRow(cell)) + " ");
            System.out.println("Timetamp:" + cell.getTimestamp() + " ");
            System.out.println("column Family:" + new String(CellUtil.cloneFamily(cell)) + " ");
            System.out.println("row Name:" + new String(CellUtil.cloneQualifier(cell)) + " ");
            System.out.println("value:" + new String(CellUtil.cloneValue(cell)) + " ");
        }
    }

    // 批量查找数据
    public void scanData(String startRow, String stopRow) throws IOException {
        if (isConnectedTable()) {
            Scan scan = new Scan();
            scan.setStartRow(Bytes.toBytes(startRow));
            scan.setStopRow(Bytes.toBytes(stopRow));
            ResultScanner resultScanner = table.getScanner(scan);
            for (Result result : resultScanner) {
                showCell(result);
            }
        }
    }

    //检查该表是否存在
    public boolean checkExistTable() throws IOException {
        if (admin.tableExists(TableName.valueOf(tableName))) {
            return true;
        }
        System.out.println("The table is not exits!");
        return false;
    }

    //检查是否已经连接上了该表
    public boolean isConnectedTable() throws IOException {
        if (null != table)
            return true;
        else if (checkExistTable()) {
            table = connection.getTable(TableName.valueOf(tableName));
            if (null != table)
                return true;
            System.out.println("The table is not connected！");
            return false;
        }
        System.out.println("The table is not exits!");
        return false;
    }
}