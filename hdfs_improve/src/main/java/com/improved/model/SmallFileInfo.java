package com.improved.model;

public class SmallFileInfo {
    //小文件文件名
    private String fileName = "";
    //合成文件所在路径(小文件存放路径)
    private String path = "";
    //合成后的大文件文件名
    private String cmbFileName = "";
    //小文件在合并后的大文件中的起始偏移量
    private String startSet = "";
    //小文件大小
    private String size = "";

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getCmbFileName() {
        return cmbFileName;
    }

    public void setCmbFileName(String cmbFileName) {
        this.cmbFileName = cmbFileName;
    }

    public String getStartSet() {
        return startSet;
    }

    public void setStartSet(String startSet) {
        this.startSet = startSet;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public boolean isOk() {
        return !(fileName.isEmpty() || path.isEmpty() || cmbFileName.isEmpty() || startSet.isEmpty() || size.isEmpty());
    }

    @Override
    public String toString() {
        return "小文件：" + fileName + ",放到" + path + "目录下的这个文件中：" + cmbFileName + "起始偏移量为:" + startSet + "，文件大小为：" + size;
    }
}
