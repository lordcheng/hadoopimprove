package com.improved.hdfs;

import com.improved.hbase.OperateTable;
import com.improved.model.SmallFileInfo;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;

/**
 * 将本地小文件合并上传到HDFS文件系统中。
 * 一种方法可以现在本地写一个脚本，先将一个文件合并为一个大文件，然后将整个大文件上传，这种方法占用大量的本地磁盘空间；
 * 另一种方法如下，在复制的过程中上传。
 */
public class OperateFile {
    private static Long blockSize;
    private static String tableName;
    private static String colFamily1;
    private static int MAX_FILE_NUM;

    private HashMap<String, SmallFileInfo> indexs;
    private OperateTable opt;
    private Configuration conf;
    private FileSystem local;
    private FileSystem fs;

    public OperateFile() throws Exception {
        init();
    }

    public void init() throws Exception {
        blockSize = 134217728L;//128M
        tableName = "CombileFileIndexs";
        colFamily1 = "CmbFileInfo";
        MAX_FILE_NUM = 90919;//测试文件数
        indexs = new HashMap<String, SmallFileInfo>();
        opt = new OperateTable(tableName, new String[]{colFamily1});
        conf = new Configuration();
        conf.set("fs.defaultFS", "hdfs://master:9000");
        local = FileSystem.getLocal(conf);
        fs = FileSystem.get(conf);
    }

    //合并上传文件并生成索引，将索引交给hbase模块上传
    public void putMergeFunc(String localDir, String fsFile) throws Exception {
        Path localdir = new Path(localDir);
        FileStatus[] status = local.listStatus(localdir);

        fsFile = fsFile.startsWith("/") ? fsFile : fsFile + "/";
        String cmbFileName = System.currentTimeMillis() + "";
        Path hdfsFile = new Path(fsFile + cmbFileName);
        FSDataOutputStream out = fs.create(hdfsFile);

        SmallFileInfo sfi = null;
        Long cmbSize = 0L;
        for (FileStatus st : status) {
            Path temp = st.getPath();
            FSDataInputStream in = local.open(temp);
            sfi = new SmallFileInfo();
            sfi.setFileName(temp.getName());
            sfi.setSize(st.getLen() + "");
            sfi.setPath(fsFile);
            if (st.getLen() + cmbSize > blockSize) {
                out.close();
                cmbFileName = System.currentTimeMillis() + "";
                hdfsFile = new Path(fsFile + "/" + cmbFileName);
                out = fs.create(hdfsFile);
                cmbSize = 0L;
                uploadIndexs(indexs);
            }
            System.out.println("小文件名：" + temp.getName() + "文件长度：" + st.getLen() + "文件起始偏移量：" + cmbSize);
            sfi.setStartSet(cmbSize + "");
            sfi.setCmbFileName(cmbFileName);
            IOUtils.copyBytes(in, out, 4096, false);
            cmbSize += st.getLen();
            indexs.put(sfi.getPath() + sfi.getFileName(), sfi);
            in.close();
        }
        out.close();
        uploadIndexs(indexs);
    }

    //上传索引到hbase
    public void uploadIndexs(HashMap<String, SmallFileInfo> indexs) throws Exception {
        opt.insterRow(colFamily1, "smallFileName", "hdfsPath",
                "combineFileName", "startSet", "len", indexs);
        indexs.clear();
    }

    //从HBase读取索引，然后根据索引从hdfs下载对应文件
    public void readByIndexs(String remotePath, String smallFileName, String localPath) throws Exception {
        String rowkey = remotePath.endsWith("/") ? remotePath + smallFileName : remotePath + "/" + smallFileName;
        Result result = opt.getData(rowkey, colFamily1);
        if (!result.isEmpty()) {
            SmallFileInfo sfi = new SmallFileInfo();
            String sfn = new String(result.getValue(Bytes.toBytes(colFamily1), Bytes.toBytes("smallFileName")));
            sfi.setFileName(sfn);
            String hdfsPath = new String(result.getValue(Bytes.toBytes(colFamily1), Bytes.toBytes("hdfsPath")));
            sfi.setPath(hdfsPath);
            String combineFileName = new String(result.getValue(Bytes.toBytes(colFamily1), Bytes.toBytes("combineFileName")));
            sfi.setCmbFileName(combineFileName);
            String startSet = new String(result.getValue(Bytes.toBytes(colFamily1), Bytes.toBytes("startSet")));
            sfi.setStartSet(startSet);
            String len = new String(result.getValue(Bytes.toBytes(colFamily1), Bytes.toBytes("len")));
            sfi.setSize(len);
            System.out.println(sfi.toString());

            if(sfi.isOk()) {
                Path pt = new Path(sfi.getPath() + "/" + sfi.getCmbFileName());
                FSDataInputStream in = fs.open(pt);
                Long st = Long.parseLong(sfi.getStartSet());
                in.seek(st);
                FileOutputStream out = new FileOutputStream(new File(localPath + "/" + sfi.getFileName()));
                IOUtils.copyBytes(in, out, Long.parseLong(sfi.getSize()), true);
                out.close();
            }else{
                System.out.println("hbase获取的索引不完整："+sfi.toString());
            }
        } else {
            System.out.println(smallFileName + "文件不存在");
        }
    }

    //上传小文件测试
    public void putFileTest(OperateFile pm, String sourceFile, String targetFile) throws Exception {
        Long start = System.currentTimeMillis();
        pm.putMergeFunc(sourceFile, targetFile);
        Long end = System.currentTimeMillis();
        System.out.println("上传文件消耗了：" + (end - start) / 60000.0 + "分钟");
    }

    //下载小文件测试
    public void readFileTest(OperateFile pm, String remotePath, String localPath) throws Exception {
        Long start = System.currentTimeMillis();
        for (int i = 0; i < MAX_FILE_NUM; i++) {
            pm.readByIndexs(remotePath, i + ".txt", localPath);
        }
        Long end = System.currentTimeMillis();
        System.out.println("下载文件消耗了：" + (end - start) / 60000.0 + "分钟");
    }

    public static void main(String[] args) throws Exception {
        OperateFile pm = new OperateFile();
        pm.putFileTest(pm, "C:\\Users\\lordchen\\Desktop\\sources\\datasets\\txt", "/test/");
        //pm.readFileTest(pm, "/test/", "C:\\Users\\lordchen\\Desktop\\sources\\datasets\\result");
    }
}